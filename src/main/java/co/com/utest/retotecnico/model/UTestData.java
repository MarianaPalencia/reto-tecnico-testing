package co.com.utest.retotecnico.model;

public class UTestData {
     private String strFirstName;
     private String strLastName;
     private String strEmail;
     private String strBirthMonth;
     private String strBirthDay;
     private String strBirthYear;
     private String strPassword;
     private String strConfirmPassword;

    public UTestData(String strFirstName, String strLastName, String strEmail, String strBirthMonth, String strBirthDay, String strBirthYear, String strPassword, String strConfirmPassword) {
        this.strFirstName = strFirstName;
        this.strLastName = strLastName;
        this.strEmail = strEmail;
        this.strBirthMonth = strBirthMonth;
        this.strBirthDay = strBirthDay;
        this.strBirthYear = strBirthYear;
        this.strPassword = strPassword;
        this.strConfirmPassword = strConfirmPassword;
    }

    public String getStrFirstName() {
        return strFirstName;
    }

    public void setStrFirstName(String strFirstName) {
        this.strFirstName = strFirstName;
    }

    public String getStrLastName() {
        return strLastName;
    }

    public void setStrLastName(String strLastName) {
        this.strLastName = strLastName;
    }

    public String getStrEmail() {
        return strEmail;
    }

    public void setStrEmail(String strEmail) {
        this.strEmail = strEmail;
    }

    public String getStrBirthMonth() {
        return strBirthMonth;
    }

    public void setStrBirthMonth(String strBirthMonth) {
        this.strBirthMonth = strBirthMonth;
    }

    public String getStrBirthDay() {
        return strBirthDay;
    }

    public void setStrBirthDay(String strBirthDay) {
        this.strBirthDay = strBirthDay;
    }

    public String getStrBirthYear() {
        return strBirthYear;
    }

    public void setStrBirthYear(String strBirthYear) {
        this.strBirthYear = strBirthYear;
    }

    public String getStrPassword() {
        return strPassword;
    }

    public void setStrPassword(String strPassword) {
        this.strPassword = strPassword;
    }

    public String getStrConfirmPassword() {
        return strConfirmPassword;
    }

    public void setStrConfirmPassword(String strConfirmPassword) {
        this.strConfirmPassword = strConfirmPassword;
    }
}
