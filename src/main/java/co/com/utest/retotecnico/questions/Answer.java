package co.com.utest.retotecnico.questions;

import co.com.utest.retotecnico.userinterface.DashboardPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class Answer implements Question<Boolean> {
    private String question;

    public Answer(String question) {
        this.question = question;
    }

    public static Answer toThe(String question) {
        return new Answer(question);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        boolean result;
        String firstName = Text.of(DashboardPage.FIRSTNAME).viewedBy(actor).asString();
        if (question.equals(firstName)) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }
}
