package co.com.utest.retotecnico.tasks;

import co.com.utest.retotecnico.userinterface.LogInPage;
import co.com.utest.retotecnico.userinterface.SignUpPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class LogIn implements Task {
    private String strEmail;
    private String strPassword;

    public LogIn(String strEmail, String strPassword) {
        this.strEmail = strEmail;
        this.strPassword = strPassword;
    }

    public static LogIn onThePage(String strEmail, String strPassword) {
        return Tasks.instrumented(LogIn.class,strEmail, strPassword);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(LogInPage.BUTTON_LOGIN),
                Enter.theValue(strEmail).into(LogInPage.INPUT_EMAIL),
                Enter.theValue(strPassword).into(LogInPage.INPUT_PASS),
                Click.on(LogInPage.BUTTON_SEND));
    }
}
