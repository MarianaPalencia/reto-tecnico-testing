package co.com.utest.retotecnico.tasks;

import co.com.utest.retotecnico.userinterface.SignUpPage;
import co.com.utest.retotecnico.userinterface.UTestPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class SignUp implements Task {

    private String strFirstName;
    private String strLastName;
    private String strEmail;
    private String strBirthMonth;
    private String strBirthDay;
    private String strBirthYear;
    private String strPassword;
    private String strConfirmPassword;

    public SignUp(String strFirstName, String strLastName, String strEmail, String strBirthMonth, String strBirthDay, String strBirthYear, String strPassword, String strConfirmPassword) {
        this.strFirstName = strFirstName;
        this.strLastName = strLastName;
        this.strEmail = strEmail;
        this.strBirthMonth = strBirthMonth;
        this.strBirthDay = strBirthDay;
        this.strBirthYear = strBirthYear;
        this.strPassword = strPassword;
        this.strConfirmPassword = strConfirmPassword;
    }

    public static SignUp onThePage(String strFirstName, String strLastName, String strEmail, String strBirthMonth, String strBirthDay, String strBirthYear, String strPassword, String strConfirmPassword) {
        return Tasks.instrumented(SignUp.class, strFirstName, strLastName, strEmail, strBirthMonth, strBirthDay, strBirthYear, strPassword, strConfirmPassword);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(SignUpPage.BUTTON_SIGNIN),
                Enter.theValue(strFirstName).into(SignUpPage.INPUT_FIRSTNAME),
                Enter.theValue(strLastName).into(SignUpPage.INPUT_LASTNAME),
                Enter.theValue(strEmail).into(SignUpPage.INPUT_EMAIL),
                SelectFromOptions.byVisibleText(strBirthMonth).from(SignUpPage.SELECT_BIRTHMONTH),
                SelectFromOptions.byVisibleText(strBirthDay).from(SignUpPage.SELECT_BIRTHDAY),
                SelectFromOptions.byVisibleText(strBirthYear).from(SignUpPage.SELECT_BIRTHYEAR),
                Click.on(SignUpPage.BUTTON_NEXT),
                Click.on(SignUpPage.BUTTON_NEXT),
                Click.on(SignUpPage.BUTTON_NEXT),
                Enter.theValue(strPassword).into(SignUpPage.INPUT_PASSWORD),
                Enter.theValue(strConfirmPassword).into(SignUpPage.INPUT_CONFIRMPASSWORD),
                Click.on(SignUpPage.CHECKBOX_TERMOFUSE),
                Click.on(SignUpPage.CHECKBOX_PRIVACYSETTING),
                Click.on(SignUpPage.BUTTON_NEXT));
    }
}
