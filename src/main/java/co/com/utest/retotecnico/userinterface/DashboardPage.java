package co.com.utest.retotecnico.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class DashboardPage {
    public static final Target FIRSTNAME = Target.the("button that shows us the form log in")
            .located(By.xpath("//div[contains(text(), 'Jhon Doe')]"));
}
