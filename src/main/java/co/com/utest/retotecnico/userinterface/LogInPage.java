package co.com.utest.retotecnico.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class LogInPage {
    public static final Target BUTTON_LOGIN = Target.the("button that shows us the form log in")
            .located(By.xpath("//a[contains(text(), 'Log In')]"));
    public static final Target BUTTON_SEND = Target.the("button that confirm data")
            .located(By.name("login"));
    public static final Target INPUT_EMAIL = Target.the("where do we write the email")
            .located(By.id("username"));
    public static final Target INPUT_PASS = Target.the("where do we write the password")
            .located(By.id("password"));
}
