package co.com.utest.retotecnico.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class SignUpPage {
    public static final Target BUTTON_SIGNIN = Target.the("button that shows us the form sing up")
            .located(By.xpath("//a[contains(@class, 'unauthenticated-nav-bar__sign-up')]"));
    public static final Target BUTTON_NEXT = Target.the("button to next from")
            .located(By.xpath("//a[contains(@class, 'btn btn-blue')]"));
    public static final Target INPUT_FIRSTNAME = Target.the("where do we write the first name")
            .located(By.id("firstName"));
    public static final Target INPUT_LASTNAME = Target.the("where do we write the last name")
            .located(By.id("lastName"));
    public static final Target INPUT_EMAIL = Target.the("where do we write the email")
            .located(By.id("email"));
    public static final Target SELECT_BIRTHMONTH = Target.the("where do we write the birth month")
            .located(By.id("birthMonth"));
    public static final Target SELECT_BIRTHDAY = Target.the("where do we write the birth day")
            .located(By.id("birthDay"));
    public static final Target SELECT_BIRTHYEAR = Target.the("where do we write the birth year")
            .located(By.id("birthYear"));
    public static final Target INPUT_PASSWORD = Target.the("where do we write the password")
            .located(By.id("password"));
    public static final Target INPUT_CONFIRMPASSWORD = Target.the("where do we write the confirm password")
            .located(By.id("confirmPassword"));
    public static final Target CHECKBOX_PRIVACYSETTING = Target.the("where do we write the confirm password")
            .located(By.id("privacySetting"));
    public static final Target CHECKBOX_TERMOFUSE = Target.the("where do we write the confirm password")
            .located(By.id("termOfUse"));
}
