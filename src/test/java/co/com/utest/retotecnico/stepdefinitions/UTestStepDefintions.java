package co.com.utest.retotecnico.stepdefinitions;

import co.com.utest.retotecnico.model.UTestData;
import co.com.utest.retotecnico.questions.Answer;
import co.com.utest.retotecnico.tasks.*;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

public class UTestStepDefintions {

    @Before
    public void setStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^that User wants to belong to the uTest community$")
    public void thatUserWantsToBelongToTheUTestCommunity(List<UTestData> utestData) throws Exception {
        OnStage.theActorCalled("User").wasAbleTo(OpenUp.thePage(), SignUp.onThePage(
                utestData.get(0).getStrFirstName(),
                utestData.get(0).getStrLastName(),
                utestData.get(0).getStrEmail(),
                utestData.get(0).getStrBirthMonth(),
                utestData.get(0).getStrBirthDay(),
                utestData.get(0).getStrBirthYear(),
                utestData.get(0).getStrPassword(),
                utestData.get(0).getStrConfirmPassword()));
    }


    @When("^he log in into the utest page$")
    public void heLogInIntoTheUtestPage(List<UTestData> utestData) throws Exception {
        OnStage.theActorInTheSpotlight().attemptsTo(OpenUp.thePage(), LogIn.onThePage(
                utestData.get(0).getStrEmail(),
                utestData.get(0).getStrPassword()
        ));
    }

    @Then("^he finds his name in the side menu$")
    public void heFindsHisNameInTheSideMenu(List<UTestData> utestData) throws Exception {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(utestData.get(0).getStrFirstName() + " " + utestData.get(0).getStrLastName())));
    }
}
