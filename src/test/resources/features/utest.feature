# Autor: Mariana Palencia
@stories
Feature: Academy Choucair
  As a user, I want to belong to the largest community of digital freelance software testers at uTest

  @scenario1
  Scenario Outline: Create a new user
    Given that User wants to belong to the uTest community
      | strFirstName   | strLastName   | strEmail   | strBirthMonth   | strBirthDay   | strBirthYear   | strPassword   | strConfirmPassword   |
      | <strFirstName> | <strLastName> | <strEmail> | <strBirthMonth> | <strBirthDay> | <strBirthYear> | <strPassword> | <strConfirmPassword> |
    When he log in into the utest page
      | strEmail   | strPassword   |
      | <strEmail> | <strPassword> |
    Then he finds his name in the side menu
      | strFirstName   | strLastName   |
      | <strFirstName> | <strLastName> |
    Examples:
      | strFirstName | strLastName | strEmail            | strBirthMonth | strBirthDay | strBirthYear | strPassword | strConfirmPassword |
      | Jhon         | Doe         | jhondoe@yopmail.com | June          | 12          | 1985         | Querty2020  | Querty2020         |
